from django.db import models

# Create your models here.
# Shoe model will contain manufacturer, name, color, picture url, and bin in wardrobe
class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    import_href= models.CharField(max_length=100)

    def __str__(self):
        return self.closet_name

class Shoe(models.Model):
    model_name = models.CharField(max_length = 100)
    color = models.CharField(max_length = 100)
    picture_url = models.URLField(null = True)
    manufacturer = models.CharField(max_length = 100)
    bin = models.ForeignKey(
        BinVO,
        related_name= "bin",
        on_delete = models.PROTECT,
    )

    def __str__(self):
        return self.model_name

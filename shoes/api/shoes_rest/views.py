from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Shoe, BinVO
import json

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties= [
        "closet_name",
        "import_href",
    ]



class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties =["model_name", "id", 'manufacturer', 'color', ]

    def get_extra_data(self, o):
        return {'bin': o.bin.closet_name}







class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "color",
        "picture_url",
        "manufacturer",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder = ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            bin_href = content['bin']
            bin = BinVO.objects.get(import_href = bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"Fam": "There's no bin number"},
                status = 400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder = ShoeDetailEncoder,
            safe = False,
        )

@require_http_methods(["GET", "DELETE"])
def api_detail_shoes(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder = ShoeDetailEncoder,
            safe=False
        )
    else:
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


# Create your views here.

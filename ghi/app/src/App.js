import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ListShoe from './ListShoe';
import ShoeForm from './ShoeForm';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';

function App(props) {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
            <Route path="hats" element={<HatList hats={props.hats}/>}/>
            <Route path="hats/new" element={<HatForm/>}/>
          <Route path="shoes">
            <Route index element={<ListShoe shoes={props.shoes} />} />
            <Route path = "new/" element={<ShoeForm />} />

          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}




export default App;

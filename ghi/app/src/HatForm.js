import React, { useEffect, useState } from 'react';

function HatForm() {
  const [locations, setLocations] = useState([]);
  const [fabric, setFabric] = useState('')
  const [styleName, setStyleName] = useState('')
  const [color, setColor] = useState('')
  const [pictureUrl, setPictureUrl] = useState('')
	const [location, setLocation] = useState('')

  const handleFabricChange = (event) => {
    const value = event.target.value;
    setFabric(value)
  }
  const handleStyleNameChange = (event) => {
    const value = event.target.value;
    setStyleName(value)
  }
  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value)
  }
	const handlePictureUrlChange = (event) => {
    const value = event.target.value;
    setPictureUrl(value)
  }
  const handleLocationChange = (event) => {
    const value = event.target.value
    setLocation(value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data ={};

		data.fabric = fabric
    data.style_name = styleName;
    data.color = color;
    data.picture_url = pictureUrl;
    data.location = location;

    const locationUrl = "http://localhost:8090/api/hats/";
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
        const newLocation = await response.json();
        console.log(newLocation)
        setFabric('');
        setStyleName('');
        setColor('');
        setPictureUrl('');
				setLocation('');
    }
  }
  const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data["locations"])

    }
  }

  useEffect(() => {
    fetchData();
  }, []);


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input onChange={handleFabricChange} value={fabric} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStyleNameChange} value={styleName} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control"/>
              <label htmlFor="style_name">Style Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="Picture Url" required type="url" name="picture_url" id="color" className="form-control"/>
              <label htmlFor="picture_url">Picture Url</label>
            </div>
            <div className="mb-3">
              <select onChange={handleLocationChange} value={location} required name="locations" id="locations" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.href} value ={location.href}>
                      {location.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm

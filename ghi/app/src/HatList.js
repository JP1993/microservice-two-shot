import React from 'react'
import { useEffect, useState} from 'react'


function HatList(props) {
	const [hats, setHats] = useState([])

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
			console.log(data)
      setHats(data.hats)
    }
  }

	useEffect(() => {
    fetchData();
  }, []);

	const deleteHat = async(id) => {
		const hatIdUrl = `http://localhost:8090/api/hats/${id}`
		const response = await fetch(hatIdUrl, {method:"DELETE"});
		if (response.ok){
			setHats(hats.filter((hat)=> hat.id !==id));
			window.location.reload(false)
		}
	}

	return (
		<div>
			<table className ="table table_stripped">
				<thead>
					<tr>
						<th>Fabric</th>
						<th>Style name</th>
						<th>Color</th>
						<th>Picture</th>
						<th>Location</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					{hats?.map(hats => {
						return (
							<tr key={hats.id}>
								<td>{ hats.fabric }</td>
								<td>{ hats.style_name }</td>
								<td>{ hats.color }</td>
								<td><img src={ hats.picture_url } alt="Hat" style={{ maxWidth: '100px', maxHeight: '100px' }} /></td>
								<td>{ hats.location }</td>
								<td>
									<button className= "btn btn-danger" onClick={() => deleteHat(hats.id)}>Delete</button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
}

export default HatList

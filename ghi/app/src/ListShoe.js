import React from 'react'
import {useEffect, useState} from 'react'

function ListShoe(props){
  const [shoes, setShoes] = useState([])

  const fetchData = async () =>{
    const url = 'http://localhost:8080/api/shoes/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes)
    }
  }


  useEffect(() => {
  fetchData();

  },[])

  const deleteShoe = async(id) => {
    const shoeUrl = `http://localhost:8080/api/shoes/${id}`
    const response = await fetch(shoeUrl, {method: "DELETE"});
    if (response.ok){
      setShoes(shoes.filter((shoe) => shoe.id !==id));
    }
  }

  return(
    <table className= "table table-striped">
        <thead>
            <tr>
                <th>Brand</th>
                <th>Model</th>
                <th>Color</th>
                <th>Bin</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
        {props.shoes?.map(shoe => {
        return (
          <tr key={shoe.id}>
            <td>{ shoe.manufacturer }</td>
            <td>{ shoe.model_name }</td>
            <td>{ shoe.color }</td>
            <td>{ shoe.bin }</td>
            <td><button className='btn btn-danger' onClick={() => deleteShoe(shoe.id)} >Delete Shoe</button></td>
          </tr>
        );
      })}
        </tbody>
    </table>
  );



}

export default ListShoe

import React, { useEffect, useState} from 'react'


export default function Form(){


    const [manufacturer, setManufacturer] = useState('');
    const [name, setName] = useState('');
    const [color, setColor] = useState('');
    const [pic, setPic] = useState('');
    const [bin, setBin] = useState('');
    const [fullBins, setFullBins] = useState([]);





    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePicChange = (event) => {
        const value = event.target.value;
        setPic(value);
    }

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.manufacturer = manufacturer;
        data.model_name = name;
        data.color = color;
        data.picture = pic;
        data.bin = bin

        const shoeUrl = `http://localhost:8080/api/shoes/`;
        const response = await fetch(shoeUrl, {method: "POST"});
        if (response.ok) {
            setManufacturer('');
            setName('');
            setColor('');
            setPic('');
            setBin('');
        };
    };
    const binsData = async () => {
        const binsUrl = "http://localhost:8100/api/bins/";
        const response = await fetch(binsUrl);

        if (response.ok){
            const data = await response.json();
            setFullBins(data.bins);
        }
    }


    useEffect(() => {
        binsData();
    }, []);




    return(
    <div>
        <h1> New shoe!!</h1>
        <form onSubmit={handleSubmit}>
            <div>
                <input value={manufacturer} onChange={handleManufacturerChange} placeholder="Manufacturer" type="text" id='manufacturer' className='form-control' />
            </div>
            <div>
                <input value={name} onChange={handleNameChange} placeholder="Model" type="text" id='model_name' name="model_name" className='form-control' />
            </div>
            <div>
                <input value={color} onChange={handleColorChange} placeholder="Color" type="text" />
            </div>
            <div>
                <input value={pic} onChange={handlePicChange} placeholder="Name" type="url" id='picture' className='form-control' />
            </div>
            <div>
                <input value={bin} onChange={handleBinChange} id='bin' className='form-select' type="text" />
                <option value=''>choose a bin</option>
                {fullBins.map(bin => {
                    return (
                        <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                    )
                })}
            </div>
            <button className='btn btn-primary'>Create</button>

        </form>
    </div>
    );

}

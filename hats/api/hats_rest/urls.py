from django.urls import path
from .views import api_list_hats, api_show_hat


urlpatterns = [
    # gets a list of hats and creates a new hat
    path("hats/", api_list_hats, name="api_list_hats"),
    # gets hat detail and can delete a specified hat
    path("hats/<int:id>/", api_show_hat, name="api_show_hat"),
]

from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .models import Hat, LocationVO

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties=[
        "closet_name",
        "import_href",
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style_name",
        "picture_url",
        "fabric",
        "color",
        "location",
		]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
		]
    encoders={
        "location": LocationVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatListEncoder, safe=False,)
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location

        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "id is invalid"},status=400,)

        hat = Hat.objects.create(**content)
        return JsonResponse({"hat":hat}, encoder=HatDetailEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_show_hat(request, id):

    if request.method == "GET":
        hat = Hat.objects.get(id=id)
        return JsonResponse({"hat": hat}, encoder=HatDetailEncoder, safe=False)

    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
